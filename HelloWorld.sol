pragma solidity ^0.4.0;
contract HelloWorld {
    string str;

    function HelloWorld(string inputString) public {
        str = inputString;
    }

    function say() public constant returns (string) {
        return str;
    }
}

